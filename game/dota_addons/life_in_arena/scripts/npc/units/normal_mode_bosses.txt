"1_wave_boss" 
{
"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_kobold/kobold_a/n_creep_kobold_a.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1.2" 
"Level" "1" 

"Ability1" "1_wave_stomp" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 
 


"ArmorPhysical" "2" 
"MagicalResistance" "0" 
 


"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "30" 
"AttackDamageMax" "30" 
"AttackRate" "1.55" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 
 


"BountyXP" "25" 
"BountyGoldMin" "35" 
"BountyGoldMax" "35" 
 


"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "300" 
"MovementTurnRate" "0.8" 
 


"StatusHealth" "250" 
"StatusHealthRegen" "0" 
"StatusMana" "300" 
"StatusManaRegen" "0" 
 


"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"
 


"VisionDaytimeRange" "800" 
"VisionNighttimeRange" "800" 
 


"HasInventory" "0" 

"PathfindingSearchDepthScale" "0.22" //0.3
//"vscripts"	"AI\1_wave_bosses.lua"

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"				"1_wave_stomp"
			"AOE"                "1"
			"Radius"             "245"
			"MinimumTargets"     "1"
		}
	}
}
}

"2_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_centaur_lrg/n_creep_centaur_lrg.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1" 
"Level" "1" 




"Ability1" "2_wave_return_aura" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "4" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "60" 
"AttackDamageMax" "60" 
"AttackRate" "1.25" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "25" 
"BountyGoldMin" "35" 
"BountyGoldMax" "35" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "350" 
"MovementTurnRate" "0.8" 



"StatusHealth" "300" 
"StatusHealthRegen" "0" 
"StatusMana" "200" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "800" 
"VisionNighttimeRange" "800" 



"HasInventory" "0" 

"PathfindingSearchDepthScale" "0.22" //0.3
//"vscripts"	"AI\attack_wave_only.lua" //

}

"3_wave_boss" 
{
"BaseClass" 	"npc_dota_creature" 
"Model" 		"models/creeps/neutral_creeps/n_creep_worg_large/n_creep_worg_large.vmdl" 
"SoundSet" 		"n_creep_Melee" 
"ModelScale" 	"1" 
"Level" 		"1" 

"precache"
{
	"model"	"models/creeps/neutral_creeps/n_creep_worg_large/n_creep_worg_large.vmdl"
}

"Ability1" "3_wave_evasion" 
"Ability2" "3_wave_rejuvenation" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 

"ArmorPhysical" 		"6" 
"MagicalResistance" 	"0" 
"AttackCapabilities" 	"DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" 		"100" 
"AttackDamageMax"		"100" 
"AttackRate" 			"1.3" 
"AttackAnimationPoint" 	"0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" 			"100" 
"ProjectileModel"	 	"" 
"ProjectileSpeed" 		"" 

"BountyXP" "25" 
"BountyGoldMin" "35" 
"BountyGoldMax" "35" 

"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "270" 
"MovementTurnRate" "0.8" 

"StatusHealth" "500" 
"StatusHealthRegen" "0" 
"StatusMana" "250" 
"StatusManaRegen" "1" 

"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"

"VisionDaytimeRange" "800" 
"VisionNighttimeRange" "800" 

"HasInventory" "0" 

"PathfindingSearchDepthScale" "0.22" //0.3
//"vscripts"	"AI\3_wave_bosses.lua"

"Creature"
	{
		"DefaultState"			"Invade"
		"States"	
			{
				"Invade"
				{
					"Name"				"Invade"
					"Aggression"		"0.0" //cast offensive spells whenever
					"Avoidance"			"0.0" //dont flee
					"Support"			"50.0" //start buffing
				}
			}
		
		"DefensiveAbilities"
			{
			    "Ability1"              
			    {
			        "Name"                "3_wave_rejuvenation"
			        "UseSelfishly"        "1"
			        "Buff"                "1"
			        "UseAtHealthPercent"  "50"
			    }
			}
	}
}

"4_wave_boss" 
{
"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_troll_dark_b/n_creep_troll_dark_b.vmdl" 
"SoundSet" "n_creep_Ranged" 
"ModelScale" "1" 
"Level" "1" 




"Ability1" "4_wave_death_coil" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "7" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_RANGED_ATTACK"
"AttackDamageMin" "80" 
"AttackDamageMax" "80" 
"AttackRate" "0.9" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "800" 
"ProjectileModel" "particles/base_attacks/ranged_badguy.vpcf" 
"ProjectileSpeed" "1200" 




"BountyXP" "25" 
"BountyGoldMin" "35" 
"BountyGoldMax" "35" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "270" 
"MovementTurnRate" "0.8" 



"StatusHealth" "650" 
"StatusHealthRegen" "0" 
"StatusMana" "300" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "800" 
"VisionNighttimeRange" "800" 



"HasInventory" "0" 

"PathfindingSearchDepthScale" "0.22" //0.3
//"vscripts"	"AI\4_wave_bosses.lua"

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"				"4_wave_death_coil"
			"Damage"            "1"
		}
	}	
	
}
}

"6_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/item_creeps/i_creep_necro_warrior/necro_warrior.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1" 
"Level" "1" 


"Ability1" "6_wave_spell_resist" 
"Ability2" "6_wave_cripple" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "6" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "150" 
"AttackDamageMax" "150" 
"AttackRate" "1.05" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "33" 
"BountyGoldMin" "40" 
"BountyGoldMax" "40" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "400" 
"MovementTurnRate" "0.6" 



"StatusHealth" "1250" 
"StatusHealthRegen" "0" 
"StatusMana" "350" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"				"6_wave_cripple"
			"IsDebuff"              "1"
		}
	}	
	
}

}

"7_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/items/furion/treant_stump.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1.5" 
"Level" "1" 

				"precache"
		{
			"model"	"models/items/furion/treant_stump.vmdl" 
		}




"Ability1" "7_wave_howl_of_terror" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "6" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "220" 
"AttackDamageMax" "220" 
"AttackRate" "0.55" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "33" 
"BountyGoldMin" "40" 
"BountyGoldMax" "40" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "500" 
"MovementTurnRate" "0.5" 



"StatusHealth" "1600" 
"StatusHealthRegen" "0" 
"StatusMana" "450" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"		 		 "7_wave_howl_of_terror"
			"AOE"                "1"
			"Radius"             "700"
			"MinimumTargets"     "1"
		}
	}
}
}

"8_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/lane_creeps/creep_bad_melee/creep_bad_melee_mega.vmdl" 
"SoundSet" "Hero_Sven" 
"ModelScale" "1.5" 
"Level" "1" 
				

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"				"8_wave_storm_bolt"
			"Damage"            "1"
			"Stun"              "1"
		}
	}	
	
}




"Ability1" "8_wave_cleave" 
"Ability2" "spell_immunity" 
"Ability3" "8_wave_lifesteal_aura" 
"Ability4" "8_wave_storm_bolt" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "8" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "250" 
"AttackDamageMax" "250" 
"AttackRate" "1.25" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "33" 
"BountyGoldMin" "50" 
"BountyGoldMax" "50" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "270" 
"MovementTurnRate" "0.5" 



"StatusHealth" "1900" 
"StatusHealthRegen" "0" 
"StatusMana" "300" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}

"9_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/heroes/morphling/morphling.vmdl" 
"SoundSet" "Hero_Morphling" 
"ModelScale" "1" 
"Level" "1" 
"ProjectileModel"           "particles/units/heroes/hero_morphling/morphling_base_attack.vpcf" 
"ProjectileSpeed"           "1200" 	
	"precache"
			{
				"model"	"models/items/morphling/ancient_armor_arms/ancient_armor_arms.mdl"
				"model"	"models/items/morphling/ancient_armor_back/ancient_armor_back.mdl"
				"model"	"models/items/morphling/ancient_armor_breastplates/ancient_armor_breastplates.mdl"
				"model"	"models/items/morphling/ancient_armor_head/ancient_armor_head.mdl"
				"model"	"models/items/morphling/ancient_armor_shoulders/ancient_armor_shoulders.mdl"
	
			}
			
	"Creature"
			{
			"AttachWearables"
				{
					"Wearable1"		{	"ItemDef"		"5442"		} 
					"Wearable2" 	{	"ItemDef"		"5443"		} 
					"Wearable3"		{	"ItemDef"		"5444"		} 
					"Wearable4"		{	"ItemDef"		"5445"		} 
					"Wearable5"		{	"ItemDef"		"5446"		} 
				}
				}
	



"Ability1" "" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "4" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_RANGED_ATTACK"
"AttackDamageMin" "280" 
"AttackDamageMax" "280" 
"AttackRate" "0.7" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "600" 




"BountyXP" "33" 
"BountyGoldMin" "45" 
"BountyGoldMax" "45" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "220" 
"MovementTurnRate" "0.8" 



"StatusHealth" "2400" 
"StatusHealthRegen" "0" 
"StatusMana" "400" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}

"11_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/items/beastmaster/boar/beast_deming/beast_deming.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1.2" 
"Level" "1" 
	"precache"
		{
			"models/items/beastmaster/boar/beast_deming/beast_deming.vmdl"
		}



"Ability1" "" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "6" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "150" 
"AttackDamageMax" "150" 
"AttackRate" "1.05" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "33" 
"BountyGoldMin" "40" 
"BountyGoldMax" "40" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "400" 
"MovementTurnRate" "0.6" 



"StatusHealth" "1250" 
"StatusHealthRegen" "0" 
"StatusMana" "350" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}

"12_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_furbolg/n_creep_furbolg_disrupter.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1" 
"Level" "1" 




"Ability1" "12_wave_bash" 
"Ability2" "12_wave_bloodlust" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "15" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "480" 
"AttackDamageMax" "480" 
"AttackRate" "0.85" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "48" 
"BountyGoldMin" "50" 
"BountyGoldMax" "50" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "300" 
"MovementTurnRate" "0.6" 



"StatusHealth" "4200" 
"StatusHealthRegen" "0" 
"StatusMana" "600" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 

"Creature"
	{
		"DefaultState"			"Invade"
		"States"	
			{
				"Invade"
				{
					"Name"				"Invade"
					"Aggression"		"0.0" //cast offensive spells whenever
					"Avoidance"			"0.0" //dont flee
					"Support"			"90.0" //start buffing
				}
			}
		
		"DefensiveAbilities"
			{
			    "Ability1"              
			    {
			        "Name"                "12_wave_bloodlust"
			        "UseSelfishly"        "1"
			        "Buff"                "1"
			        "UseAtHealthPercent"  "90"
			    }
			}
	}
}

"13_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_golem_a/neutral_creep_golem_a.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1" 
"Level" "1" 




"Ability1" "spell_immunity" 
"Ability2" "13_wave_aura" 
"Ability3" "13_wave_hurl_boulder" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "20" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "500" 
"AttackDamageMax" "500" 
"AttackRate" "1" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "48" 
"BountyGoldMin" "50" 
"BountyGoldMax" "50" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "270" 
"MovementTurnRate" "0.6" 



"StatusHealth" "3500" 
"StatusHealthRegen" "0" 
"StatusMana" "800" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"				"13_wave_hurl_boulder"
			"Damage"            "1"
			"Stun"              "1"
			"IsDebuff"			"1"
		}
	}	
	
}
}

"14_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_thunder_lizard/n_creep_thunder_lizard_big.vmdl" 
"SoundSet" "n_creep_Ranged" 
"ModelScale" "1" 
"Level" "1" 
"ProjectileModel"           "particles/base_attacks/ranged_badguy.vpcf" 
"ProjectileSpeed"           "1800" 



"Ability1" "14_wave_storm_bolt" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "20" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_RANGED_ATTACK"
"AttackDamageMin" "500" 
"AttackDamageMax" "500" 
"AttackRate" "1" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "500" 



"BountyXP" "48" 
"BountyGoldMin" "45" 
"BountyGoldMax" "45" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "270" 
"MovementTurnRate" "0.8" 



"StatusHealth" "3500" 
"StatusHealthRegen" "0" 
"StatusMana" "800" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 

"Creature"
{
	"DefaultState"			"Invade"
	"States"	
	{
		"Invade"
		{
			"Name"				"Invade"
			"Aggression"		"90.0" //cast offensive spells whenever
			"Avoidance"			"0.0" //dont flee
			"Support"			"0.0" //start buffing
		}
	}
	
	"OffensiveAbilities"
	{
		"Ability1"
		{
			"Name"				"14_wave_storm_bolt"
			"Damage"            "1"
			"Stun"              "1"
		}
	}	
	
}
}

"16_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_ghost_a/n_creep_ghost_a.vmdl" 
"SoundSet" "n_creep_Ranged" 
"ModelScale" "1.5" 
"Level" "1" 
"ProjectileModel"           "particles/base_attacks/ranged_badguy_launch_b.vpcf" 
"ProjectileSpeed"           "500" 



"Ability1" "" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "30" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_RANGED_ATTACK"
"AttackDamageMin" "750" 
"AttackDamageMax" "750" 
"AttackRate" "1" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "400" 
"ProjectileModel" "" 
"ProjectileSpeed" "1200" 



"BountyXP" "63" 
"BountyGoldMin" "50" 
"BountyGoldMax" "50" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "270" 
"MovementTurnRate" "0.8" 



"StatusHealth" "4500" 
"StatusHealthRegen" "0" 
"StatusMana" "800" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}

"17_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_vulture_a/n_creep_vulture_a.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1.5" 
"Level" "1" 


"Ability1" "17_wave_evasion" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "66" 
"MagicalResistance" "50" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "900" 
"AttackDamageMax" "900" 
"AttackRate" "0.85" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "63" 
"BountyGoldMin" "60" 
"BountyGoldMax" "60" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "350" 
"MovementTurnRate" "0.5" 



"StatusHealth" "5200" 
"StatusHealthRegen" "0" 
"StatusMana" "400" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}

"18_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/heroes/viper/viper.vmdl" 
"SoundSet" "n_creep_Ranged" 
"ModelScale" "1.4" 
"Level" "1" 
"ProjectileModel" "particles/units/heroes/hero_viper/viper_base_attack.vpcf" 
"ProjectileSpeed" "1200" 
	"precache"
		{
			"model" "models/heroes/viper/viper.vmdl"
			"particle" "particles/units/heroes/hero_viper/viper_base_attack.vpcf"
		}



"Ability1" "18_megaboss_bash" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "30" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_RANGED_ATTACK"
"AttackDamageMin" "1400" 
"AttackDamageMax" "1400" 
"AttackRate" "1.5" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "250" 




"BountyXP" "63" 
"BountyGoldMin" "55" 
"BountyGoldMax" "55" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "350" 
"MovementTurnRate" "0.8" 



"StatusHealth" "5500" 
"StatusHealthRegen" "0" 
"StatusMana" "0" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}

"19_wave_boss" 
{


"BaseClass" "npc_dota_creature" 
"Model" "models/creeps/neutral_creeps/n_creep_satyr_a/n_creep_satyr_a.vmdl" 
"SoundSet" "n_creep_Melee" 
"ModelScale" "1" 
"Level" "1" 




"Ability1" "" 
"Ability2" "" 
"Ability3" "" 
"Ability4" "" 
"Ability5" "" 
"Ability6" "" 
"Ability7" "" 
"Ability8" "" 



"ArmorPhysical" "35" 
"MagicalResistance" "0" 



"AttackCapabilities" "DOTA_UNIT_CAP_MELEE_ATTACK"
"AttackDamageMin" "998" 
"AttackDamageMax" "998" 
"AttackRate" "2.05" 
"AttackAnimationPoint" "0.3" 
"AttackAcquisitionRange" "10000" 
"AttackRange" "100" 
"ProjectileModel" "" 
"ProjectileSpeed" "" 



"BountyXP" "63" 
"BountyGoldMin" "65" 
"BountyGoldMax" "65" 



"MovementCapabilities" "DOTA_UNIT_CAP_MOVE_GROUND" 
"MovementSpeed" "320" 
"MovementTurnRate" "0.5" 



"StatusHealth" "7000" 
"StatusHealthRegen" "0" 
"StatusMana" "1000" 
"StatusManaRegen" "1" 



"TeamName" "DOTA_TEAM_BADGUYS" 
"CombatClassAttack" "DOTA_COMBAT_CLASS_ATTACK_BASIC"
"CombatClassDefend" "DOTA_COMBAT_CLASS_DEFEND_STRONG"
"UnitRelationshipClass" "DOTA_NPC_UNIT_RELATIONSHIP_TYPE_DEFAULT"



"VisionDaytimeRange" "10000" 
"VisionNighttimeRange" "10000" 



"HasInventory" "0" 
}